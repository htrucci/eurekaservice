package com.htrucci.eurekaservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@EnableDiscoveryClient
@SpringBootApplication
public class EurekaserviceApplication implements CommandLineRunner {

	@Autowired
	private RestTemplate restTemplate;

	public static void main(String[] args) {
		SpringApplication.run(EurekaserviceApplication.class, args);
	}


	@Configuration
	class AppConfiguration{
		@LoadBalanced
		@Bean
		RestTemplate restTemplate(){
			return new RestTemplate();
		}
	}

	@Override
	public void run(String... strings) throws Exception {
		System.out.print("bootstrapping server....");

		restTemplate.getForObject("http://apigateway/api/test", String.class);

	}

}
